import os
os.system("python3 -m pip install -r /opt/ml/processing/input/env/requirements.txt")

import pandas as pd
import logging

logging.basicConfig(
    level=logging.INFO, 
    format= '[%(asctime)s] {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s',
    datefmt='%H:%M:%S'
)

DATA_PATH = f"/opt/ml/processing/input/data/20_newsgroup_train.csv"
CORPUS_EXPORT_PATH = "/opt/ml/processing/output/20_newsgroup_train_corpus.txt"

def process_main(dependent_data_path=DATA_PATH, corpus_export_path=CORPUS_EXPORT_PATH):

    os.makedirs(os.path.dirname(CORPUS_EXPORT_PATH), exist_ok=True)

    logging.info("Process the train dataset")
    assert os.path.exists(dependent_data_path), f"Dependent dataset doesn't exist (path: {dependent_data_path})"
    data = pd.read_csv(dependent_data_path).dropna()
    logging.info("Successfully loaded the data, now starts preprocessing")
    encoded_series = "__label__" + data["title"] + " " + data["text"]
    encoded_series = encoded_series.rename("encoded")
    logging.info(f"Showing the Encoded Rows \n{encoded_series}")
    
    with open(corpus_export_path, mode="w") as f:
        exported = 0
        for idx, value in encoded_series.items():
            try:
                f.write(value + "\n")
            except:
                logging.error(f"Failed to Export Row: {idx} Value: {value}")
            finally:
                exported += 1
                if exported % 1000 == 0 or exported == encoded_series.shape[0]:
                    logging.info(f"Exported {exported} Records")

if __name__ == "__main__":
    process_main()