import os
os.system("python3 -m pip install -r /opt/ml/processing/input/env/requirements.txt")

from sklearn.datasets import fetch_20newsgroups
from sklearn.model_selection import train_test_split
import pandas as pd
import logging
import string

logging.basicConfig(
    level=logging.INFO, 
    format= '[%(asctime)s] {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s',
    datefmt='%H:%M:%S'
)

def preprocess(text):
    return " ".join(
        [
            token for token in text.strip().lower().replace("\n", " ").split() 
            if any([c.isalpha() for c in token])
        ]
    )
def twenty_newsgroup_to_csv(subset_name='train', output_dir="./"):

    os.makedirs(output_dir, exist_ok=True)

    logging.info(f"Acquire {subset_name} Data")
    newsgroups_data = fetch_20newsgroups(subset=subset_name, remove=('headers', 'footers', 'quotes'))

    df = pd.DataFrame([newsgroups_data.data, newsgroups_data.target.tolist()]).T
    df.columns = ['text', 'target']
    df["text"] = df["text"].map(preprocess)

    targets = pd.DataFrame(newsgroups_data.target_names)
    targets.columns=['title']

    out = pd.merge(df, targets, left_on='target', right_index=True).dropna(subset=['text'])
    logging.info(f"\n {out.head(2).round(3)}")
    
    out.to_csv(os.path.join(output_dir, f'20_newsgroup_{subset_name}.csv'), index=False)
    print(f"Dataset Describe \n{out.describe(include='object')}")

twenty_newsgroup_to_csv('train', '/opt/ml/processing/output/')
twenty_newsgroup_to_csv('test', '/opt/ml/processing/output/')