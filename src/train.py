import os
import pandas as pd
import logging
import fasttext

import argparse

logging.basicConfig(
    level=logging.INFO, 
    format= '[%(asctime)s] {%(pathname)s:%(lineno)cod} %(levelname)s - %(message)s',
    datefmt='%H:%M:%S'
)

# /opt/ml/input/data/<channel_name>/<file_name>
CORPUS_PATH = "/opt/ml/input/data/data/20_newsgroup_train_corpus.txt"
# all output will goes to /opt/ml/model/
MODEL_PATH = "/opt/ml/model/ft_model.ftz"

def train_eval_main(corpus_path=CORPUS_PATH, model_path=MODEL_PATH, params={}):

    os.makedirs(os.path.dirname(model_path), exist_ok=True)

    logging.info("Train on trainset")
    model = fasttext.train_supervised(
        corpus_path, 
        thread=os.cpu_count(), 
        **params
    )
    logging.info(f"Model ingested unique words: {model.words.__len__()}")
    logging.info(f"Model trained against unique labels: {model.labels.__len__()}")
    model.save_model(model_path)
    

if __name__  == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-v", "--verbose",
        type=int,
        default=2
    )
    parser.add_argument(
        "-e", "--epoch",
        type=int,
        default=10
    )
    parser.add_argument(
        "--minn",
        type=int,
        default=3
    )
    parser.add_argument(
        "--maxn",
        type=int,
        default=6
    )
    parser.add_argument(
        "--lr",
        type=float,
        default=0.1
    )
    parser.add_argument(
        "--minCount",
        type=int,
        default=1
    )
    parser.add_argument(
        "--neg",
        type=int,
        default=5
    )
    parser.add_argument(
        "--loss",
        type=str,
        default="softmax"
    )
    args = parser.parse_args()
    params = {
        "verbose": args.verbose,
        "epoch": args.epoch,
        "minn": args.minn,
        "maxn": args.maxn,
        "lr": args.lr,
        "minCount": args.minCount,
        "neg": args.neg,
        "loss": args.loss
    }
    train_eval_main(params=params)