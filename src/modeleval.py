import os
os.system("python3 -m pip install -r /opt/ml/processing/input/env/requirements.txt")

import logging
from sklearn.metrics import classification_report
import pandas as pd
import fasttext
from functools import partial
import datetime
import tarfile
import argparse
import json

logging.basicConfig(
    level=logging.INFO, 
    format= '[%(asctime)s] {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s',
    datefmt='%H:%M:%S'
)

MODEL_PACK_PATH = "/opt/ml/processing/input/model/model.tar.gz"
MODEL_PATH = "/opt/ml/processing/input/model/ft_model.ftz"
TESTSET_PATH = "/opt/ml/processing/input/data/20_newsgroup_test.csv"
MODEL_EVAL_REPORT_PATH = "/opt/ml/processing/output/eval_report/model_eval_report.txt"
MODEL_EVAL_METRICS_PATH = "/opt/ml/processing/output/metrics/model_eval_metrics.json"

# unzip the model pack
print(f"Unzip Model Package to {os.path.dirname(MODEL_PATH)}")
with tarfile.open(MODEL_PACK_PATH) as model_pack:
    model_pack.extractall(os.path.dirname(MODEL_PATH)) # specify which folder to extract to

def inference(text, model, k=1):
    try:
        preds, scores = model.predict(text, k=k, threshold=0)
        resp = [(pred, score) for pred, score in zip(preds, scores)]
        if len(resp) == 0:
            logging.debug(f"Failed to get prediction on text: {text}")
            return [("<NoResult>", None)]
        else:
            return resp
    except:
        logging.debug(f"Failed to get prediction on text: {text}")
        return [("<NoResult>", None)]

def process_main(model_path=MODEL_PATH, test_set_path=TESTSET_PATH, 
                eval_report_path=MODEL_EVAL_REPORT_PATH, 
                eval_metrics_path=MODEL_EVAL_METRICS_PATH,
                eval_k=1):

    model = fasttext.load_model(model_path)
    logging.info("Start Model Eval")  
    test_data = pd.read_csv(test_set_path).dropna()
    loaded_infer = partial(inference, model=model, k=eval_k)
    test_data["preds"] = test_data["text"].map(lambda x: loaded_infer(x))
    test_data["1st_pred"] = test_data["preds"].map(lambda x: x[0][0].replace("__label__", ""))
    ## setup eval metrics
    logging.info("Finished acquire test set prediction, making report and metrics")
    clf_report = classification_report(test_data["title"], test_data["1st_pred"], output_dict=True)
    clf_report_df = pd.DataFrame(clf_report).transpose()
    metrics = {
        "Test-set": {
            "size": test_data.shape[0],
            "#label": test_data.title.nunique(),
        },
        "Model": {
            "Accuracy": clf_report_df.loc["accuracy", "precision"],
            "Macro-Precision": clf_report_df.loc["macro avg", "precision"],
            "Macro-Recall": clf_report_df.loc["macro avg", "recall"],
            "Weighted-Precision": clf_report_df.loc["weighted avg", "precision"],
            "Weighted-Recall": clf_report_df.loc["weighted avg", "recall"],
        }
    }
    with open(eval_report_path, mode="w") as f:
        f.write("## Model Evaluation Report\n")
        f.write("### Basic Eval Info\n")
        f.write(f"Model Evaluation Date: {datetime.datetime.now().strftime(format='%Y-%m-%d %H:%M:%S')}\n")
        f.write(f"Test set size: {test_data.shape[0]}\n")
        f.write("### Eval Metrics\n")
        f.write(clf_report_df.to_markdown())
        f.write("### Eval Metrics\n")
        f.write(
            f'''
            """\n
            {json.dumps(metrics, indent=2)}\n
            """\n
            '''
        )

    with open(eval_metrics_path, mode="w") as f:
        json.dump(metrics, f, indent=2)

    logging.info(f"Displaying meta eval results: \n{test_data.head()}")
    logging.info("Model eval complete!")

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-k", "--eval_k", 
        help="eval x-number of predictions",
        type=int,
    )

    args = parser.parse_args()
    process_main(eval_k=args.eval_k)