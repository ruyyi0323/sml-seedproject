import os
os.system("python3 -m pip install -r /opt/ml/processing/input/env/requirements.txt")

import pandas as pd
import logging
import datetime
import numpy as np

logging.basicConfig(
    level=logging.INFO, 
    format= '[%(asctime)s] {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s',
    datefmt='%H:%M:%S'
)

DATA_PATH = "/opt/ml/processing/input/data/20_newsgroup_train.csv"
EVAL_RESULT_EXPORT_PATH = "/opt/ml/processing/output/data_eval_report.txt"

def process_main(dependent_data_path=DATA_PATH, eval_result_export_path=EVAL_RESULT_EXPORT_PATH, display_cutoff=100):

    os.makedirs(os.path.dirname(eval_result_export_path), exist_ok=True)
    assert os.path.exists(dependent_data_path), f"Dependent dataset doesn't exist (path: {dependent_data_path})"
    data = pd.read_csv(dependent_data_path).dropna()
    logging.info("Successfully loaded the data, now starts data eval")
    with open(eval_result_export_path, mode="w") as f:
        f.write("## Data Evaluation Report\n\n")
        f.write("### Basic Dataset Info\n")
        f.write(f"Evaluation Date: {datetime.datetime.now().strftime(format='%Y-%m-%d %H:%M:%S')}\n")
        f.write(f"Shape of Dataset: {data.shape}\n")
        f.write("### Label Distribution\n")
        stats = data.groupby(['target', 'title']).agg(
            {
                'text': [
                    'count', 
                    lambda x: x.str.len().mean(),
                    lambda x: x.str.replace("\n", " ").str.split(" ").str.len().mean()
                ]
            }
        ).reset_index()
        stats.columns = ["target", "title", "#samples", "avg_#chars", "avg_#tokens"]
        f.write(f"\n{stats.to_markdown(index=False)}\n")
        f.write("### Head of Dataset\n\n")
        display_df = data.head().copy()
        display_df.loc[:, ["title", "text"]] = display_df.loc[:, ["title", "text"]] \
            .applymap(lambda x: x[:300] + "..." if len(x) > 300 else x)
        f.write(f"Head of Dataset: \n{display_df.to_markdown(index=False)}\n")
        logging.info("Finished export data evaluation report")

if __name__ == "__main__":
    process_main()
