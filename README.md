## SML-seedproject

This is a repo that build up a simple machine learning project pipeline through CML + Gitlab CI/CD + AWS SageMaker. There are multiple reason and limitation about why I am trying out this technical stack, don't ask.
But here is how it goes.

Project is about:
- Train a FastText Classifier model using news20groups dataset downloaded through scikit-learn

Pipeline is about:
- Managed by Gitlab CI/CD
- Use CML to publish the evaluation report
- For each stage, submit the AWS SageMaker Processing and Training Job

What's inside
- `src` folder contains the code for the pipeline, some paths are corresponding to the AWS Processing Job Naming Rules
- `build` include the util scripts that used for each stage in the pipeline, and before the experiment setup, in normal cases, there is no need to change this part
- `.gitlab-ci.yml` is the file used to manage the CI/CD flow

## Usage

### Setup

- **Clone Repo**: 
    - Use `git clone` to clone this repo to your development instance
- **Setup Repo Access Token for Gitlab**: 
    - This repo uses CML as the techonology to post the intermediate evaluation results, to enable CML access the Repo, we would need to setup Access Token for it.
        - Go to your repo in Gitlab, Settings > Access Tokens, Create a Access Token that has at least `read_repository` and `write_repository` access, and having the role `Maintainer`, create the project access code and temporarily save it.
        - Go to your CI/CD Settings > Variables of your repo, add variable called `REPO_TOKEN`, add the project access code you just created in here, tick the `Mask Variable` option and add this variable
- **Setup AWS Configuration** (Setup `AWS_ROLE`, `AWS_DEFAULT_REGION`, `AWS_DEFAULT_BUCKET`): 
    - There is a role created to be used for Gitlab Runner to attach the AWS resources, go to CI/CD Settings > Variables of your repo, add variable called `AWS_ROLE`, add the role ARN to it, and tick the `Mask Variable` option
    - Still in CI/CD Settings > Variables of your repo, add variable called `AWS_DEFAULT_REGION`, add the AWS region name for it, and **DON'T** tick the `Mask Variable` option
    - Specify an S3 Bucket to save your experiment artifact, you can use one that is already created or create one that specifically for you bucket. Go to CI/CD Settings > Variables of your repo, add variable called `AWS_DEFAULT_BUCKET`, add the bucket name to it, and **DON'T** tick the `Mask Variable` option
        - Each experiment would have all the artifacts being stored in `s3://{default_exp_bucket}/{project_name}/{commit_sha}/`
        - This part is referenced to `build/exp_utils.py` L79-83


### Development and Debugging

- **Principles**:
    - The `src` folder is where you want you write your codes in
    - For processing job, like feature engineering, data preparation / preprocess
        - only specific script will be taken, for your convenience, please try to make this script as independent as possible, which means no script dependency from other scripts in the src folder
        - for processing job scripts, we suggest the final input and output file can be stored in `/opt/ml/processing/input/<input channel>` and `/opt/ml/processing/output/<output channel>`, correspondingly
    - For training job, the whole src folder will be taken, your training script can have script dependency
        - for training job, all the inputs are going to `opt/ml/input/data/<input channel>`, your code will goes to `/opt/ml/input/code/`

- **Local Development**:
    - Local development is mainly a stage using sagemaker local mode to run your scripts, so you may want to make sure you're starting with a compatitble size of dataset (not too big) or you can handle the dataset with batch transforming (by using `dask` or `pySpark` etc.) in your local instance
    - Modify the `pipeline_run_template.sh` and setup the `pipeline_run.sh` by yourself, the role, default bucket and region being used to run `exp_utils.py` is highly suggested to be the same that you have in step `Setup AWS Configuration`, unless you have a different setup intentionally.
    - This milestone can be announced as success when you are able to run through the `bash localrun.sh` command and successfully finish all your processing/training/eval ML process. (Register model is not required here)

- **Remote Development**:
    - Remote development is mainly a stage where we use `gitlab-ci.yml` to control the ML procedures, this auto control procedure will make the whole experiment replicable
    - You may want to review the `.gitlab-ci.yml` included all the steps of the pipeline that you want to run
        - The repo that you've forked has a specific set of steps used in .gitlab-ci.yml, if you changed the script name, or you want to introduce / remove some steps, you may want to modify the `.gitlab-ci.yml` correspondingly
        - In this project, we've introduced CML techonology for making comments in the commit to show the evaluation results. Which means, if you have evaluation results, you can use the `python build/cml_post_file.py --stage <stageName> --output_channel <channelName>` to export the eval file in comments, It's suggested that the format of the export eval report file to be markdown
        - Once you're ready with the `.gitlab-ci.yml`, simply commit the code then you'll have the pipeline runs with the setup gitlab runner.

## Backstage Flow Overview

TBW