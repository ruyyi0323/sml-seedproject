import argparse
import os
from exp_utils import (
    AWSSMGitExperiment,
)

EXP_CONFIG_PATH = "artifacts/exp.json"
EXP = AWSSMGitExperiment.load(EXP_CONFIG_PATH)
DOWNLOADED_EVAL_RESULT_DIR = "./downloaded_eval_results/"

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-s", "--stage", 
        help="current stage that this pipeline is in",
        type=str
    )
    parser.add_argument(
        "-c", "--output_channel", 
        help="the output channel of that stage",
        type=str,
        default="eval_result"
    )
    parser.add_argument(
        "-f", "--file_name", 
        help="specific file name in the channel that you want to export",
        type=str,
        default=None
    )

    args = parser.parse_args()
    export_dir = EXP.config["stages"][args.stage]["outputs"][args.output_channel]["destination"]
    os.system(f"rm -rf {DOWNLOADED_EVAL_RESULT_DIR}/*")
    os.makedirs(DOWNLOADED_EVAL_RESULT_DIR, exist_ok=True)
    copy_cmd = f"aws s3 cp {export_dir} {DOWNLOADED_EVAL_RESULT_DIR} --recursive"
    print(f"Run {copy_cmd}")
    os.system(copy_cmd)
    if args.file_name is None:
        # if there is not file name specified, use the first file in the output channel folder
        eval_result_filepath = os.path.join(
            DOWNLOADED_EVAL_RESULT_DIR,
            [fname for fname in os.listdir(DOWNLOADED_EVAL_RESULT_DIR)][0]
        )
        print(f"Push the {eval_result_filepath} through CML")
        os.system(f"cat {eval_result_filepath} >> report.md")
    else:
        # otherwise use the specific file
        eval_result_filepath = os.path.join(
            DOWNLOADED_EVAL_RESULT_DIR,
            args.file_name
        )
        print(f"Push the {eval_result_filepath} through CML")
        os.system(f"cat {eval_result_filepath} >> report.md")
    os.system("cml-send-comment report.md")