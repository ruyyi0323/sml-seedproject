from concurrent.futures import process
from sagemaker.processing import (
    ScriptProcessor, 
)
from sagemaker.mxnet.estimator import MXNet, MXNetModel
from sagemaker.tensorflow.estimator import TensorFlow, TensorFlowModel
from sagemaker.pytorch.estimator import PyTorch, PyTorchModel
from sagemaker.huggingface.estimator import HuggingFace, HuggingFaceModel
from sagemaker.algorithm import AlgorithmEstimator
from sagemaker.chainer import Chainer, ChainerModel
from sagemaker.sklearn import SKLearn, SKLearnModel
import argparse
from exp_utils import (
    AWSSMGitExperiment,
)
from sagemaker.processing import (
    ScriptProcessor, 
    ProcessingInput, 
    ProcessingOutput
)
from sagemaker.inputs import (
    TrainingInput,
    TransformInput,
    CreateModelInput
)
import boto3
import sagemaker

EXP_CONFIG_PATH = "artifacts/exp.json"
EXP = AWSSMGitExperiment.load(EXP_CONFIG_PATH)
FRAMEWORK_ESTIMATOR_DICT = {
    "tensorflow": TensorFlow,
    "pytorch": PyTorch,
    "mxnet": MXNet,
    "chainer": Chainer,
    "huggingface": HuggingFace,
    "algorithm": AlgorithmEstimator,
    "sklearn": SKLearn,
    "scikit-learn": SKLearn
}

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-s", "--stage", 
        help="current stage that this pipeline is in",
        type=str
    )

    def get_processing_step_config(exp, stage):
        """generate a stage config on ScriptProcessor for sagemaker"""
        boto_session = boto3.Session(region_name=exp.region)
        sagemaker_session = sagemaker.Session(boto_session=boto_session)
        processor_config = exp.exp_config["stages"][stage]["processor_config"]
        run_config = exp.exp_config["stages"][stage]["run_config"]
        inputs = exp.exp_config["stages"][stage]["inputs"]
        outputs = exp.exp_config["stages"][stage]["outputs"]
        processor_config["sagemaker_session"] = sagemaker_session
        run_config.update(
            {
                "inputs": [
                    ProcessingInput(
                        source=v["source"],
                        destination=v["destination"],
                        input_name=k,
                    ) for k, v in inputs.items()
                ],
                "outputs": [
                    ProcessingOutput(
                        source=v["source"],
                        destination=v["destination"],
                        output_name=k,
                    ) for k, v in outputs.items()
                ]
            }
        )
        if exp.local is True:
            processor_config["instance_type"] = "local"
            processor_config["instance_count"] = 1
            processor_config["sagemaker_session"] = \
                sagemaker.LocalSession(boto_session=boto_session)
        return processor_config, run_config

    def get_training_step_config(exp, stage):
        """generate a stage config on Different Estimator used in sagemaker"""
        boto_session = boto3.Session(region_name=exp.region)
        sagemaker_session = sagemaker.Session(boto_session=boto_session)
        estimator_config = exp.exp_config["stages"][stage]["estimator_config"]
        run_config = exp.exp_config["stages"][stage]["run_config"]
        inputs = exp.exp_config["stages"][stage]["inputs"]
        estimator_config["sagemaker_session"] = sagemaker_session
        run_config.update(
            {
                "inputs": {
                    k: TrainingInput(
                        s3_data=v["source"],
                    ) for k, v in inputs.items()
                }
            }
        )
        if exp.local is True:
            estimator_config["instance_type"] = "local"
            estimator_config["instance_count"] = 1
            estimator_config["sagemaker_session"] = \
                sagemaker.LocalSession(boto_session=boto_session)
        return estimator_config, run_config

    args = parser.parse_args()
    job_type = EXP.exp_config["stages"][args.stage]["type"]
    if job_type == "processing":
        processor_config, run_config = get_processing_step_config(EXP, args.stage)
        processor = ScriptProcessor(**processor_config)
        processor.run(**run_config)
    elif job_type == "training":
        framework = EXP.exp_config["stages"][args.stage]["framework"]
        estimator_config, run_config = get_training_step_config(EXP, args.stage)
        estimator = FRAMEWORK_ESTIMATOR_DICT[framework](**estimator_config)
        estimator.fit(**run_config)