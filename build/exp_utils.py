import os
import json
import yaml
import boto3
import datetime
import argparse

class AWSSMGitExperiment(object):

    _allow_job_type = ["processing", "training", "register"]
    _retain_stage_config_attrs = [
        "processor_config", "estimator_config", 
        "run_config", "inputs", "outputs", 
        "model_artifact_path", "type", "framework"
    ]

    def __init__(self, role=None, default_exp_bucket=None, commit_sha=None, region="us-east-1", local=False):
        init_time = datetime.datetime.now()
        self.init_time_in_job = init_time.strftime("%Y-%m-%d-%H-%M-%S")
        self.init_time_readable = init_time.strftime("%Y/%m/%d %H:%M:%S")
        print(f"Initiate Experiment Object in {self.init_time_readable}")
        self.role = role
        self.default_exp_bucket = default_exp_bucket
        self.region = region
        self.commit_sha = commit_sha or "DEBUGUSE"
        self.exp_config = {}
        self.local = local
        self.exp_config_readed = False
        if self.local is True:
            print("Using Local Mode to Initiate this Experiment")

    def read_exp_config(self, config_path):
        assert os.path.exists(config_path)
        config = yaml.safe_load(open(config_path, mode="r"))
        self._parse_config(config)
        self.exp_config_readed = True
        return self

    def export(self, export_path):
        with open(export_path, mode="w") as f:
            json.dump(self.__dict__, f, indent=2)
            print("========== Experiment Config ==========")
            print(json.dumps(self.__dict__, indent=2))

    @classmethod
    def load(cls, load_path):
        with open(load_path, mode="r") as f:
            config = json.load(f)
        obj = cls()
        obj.__dict__ = config
        print(f"Loading Existing Experiment Config, Override Init time to {obj.init_time_readable}")
        return obj

    def _tag_converts(self, tag_dict):
        return [{"Key": k, "Value": v} for k, v in tag_dict.items()]

    def _parse_config(self, config):
        ## parse global settings
        self.exp_config["project_info"] = config["project_info"]
        self.exp_config["init_time"] = self.init_time_in_job
        self.exp_config["exp_dir"] = "s3://{}/{}/{}".format(
            self.default_exp_bucket,
            self.exp_config["project_info"]["project_name"], 
            self.commit_sha[:8]
        )
        
        ## parse parameters
        self.exp_config["parameters"] = {}
        for param_name, setup_dict in config["parameters"].items():
            self.exp_config["parameters"][param_name] = os.environ.get(
                setup_dict["name"], 
                setup_dict["default"]
            )
        ## parse stage settings
        self.exp_config["stages"] = {}
        for stage in config["stages"]:
            ## copy the config first, and then recify the paths
            stage_config = config["stages"][stage]
            parsed_stage_config = self._parse_stage_config(stage, stage_config)
            self.exp_config["stages"][stage] = parsed_stage_config
        return

    def _parse_stage_config(self, stage, stage_config):
        assert "type" in stage_config, f"You need to provide [type] for the stage [{stage}]"
        job_type = stage_config["type"]
        job_name = "{}-{}-{}-{}".format(
                self.exp_config["project_info"]["project_name"], 
                self.commit_sha[:8], 
                stage,
                self.exp_config["init_time"]
            )
        assert job_type in self._allow_job_type, f"Only following job type is allowed {self._allow_job_type}"
        # general processor/estimator settings
        if job_type == "processing":
            # processor specific settings
            stage_config["processor_config"] = {
                "role": self.role,
                "image_uri": self._parse_ref(stage_config.get("image_uri", None)),
                "instance_count": self._parse_ref(stage_config.get("instance_count", 1)),
                "instance_type": self._parse_ref(stage_config.get("instance_type", None)),
                "volume_size_in_gb": self._parse_ref(stage_config.get("volume_size", 30)),
                "output_kms_key": self._parse_ref(stage_config.get("kms_key", None)),
                "max_runtime_in_seconds": self._parse_ref(stage_config.get("max_runtime", 86400)),
                "sagemaker_session": "TBD",
                "env": self._parse_ref(stage_config.get("env", {})),
                "tags": self.exp_config["project_info"].get("tags", []),
                "command": self._parse_ref(stage_config.get("command", ["python3"]))
            }
            stage_config["run_config"] = {
                "code": stage_config["code"],
                "arguments": stage_config.get("arguments", None), 
                "wait": True,
                "job_name": job_name
            }
            ## type convert for processing job arguments
            ## job arguments has to be list[str]
            if stage_config["run_config"]["arguments"] is not None:
                stage_config["run_config"]["arguments"] = [
                    str(self._parse_ref(item)) for item in stage_config["run_config"]["arguments"]
                ]
            # parse input and output file paths
            for input_name, input_item in stage_config["inputs"].items():
                stage_config["inputs"][input_name]["source"] = self._parse_ref(
                    input_item["source"]
                )
                stage_config["inputs"][input_name]["destination"] = self._parse_ref(
                    input_item["destination"]
                )
            for output_name, output_item in stage_config["outputs"].items():
                stage_config["outputs"][output_name]["source"] = self._parse_ref(
                    output_item["source"]
                )
                stage_config["outputs"][output_name]["destination"] = self._parse_ref(
                    output_item["destination"]
                )
            if self.local is True:
                stage_config["processor_config"]["instance_type"] = "local"
                stage_config["processor_config"]["instance_count"] = 1
            # validation of config
            pass
        elif job_type == "training":
            # estimator specific settings
            stage_config["estimator_config"] = {
                "role": self.role,
                "image_uri": self._parse_ref(stage_config.get("image_uri", None)),
                "instance_count": self._parse_ref(stage_config.get("instance_count", 1)),
                "instance_type": self._parse_ref(stage_config.get("instance_type", None)),
                "volume_size": self._parse_ref(stage_config.get("volume_size", 30)),
                "max_run": self._parse_ref(stage_config.get("max_runtime", 86400)),
                "output_path": self._parse_ref(stage_config.get("output_path", None)),
                "code_location": self._parse_ref(stage_config.get("code_location", None)),
                "model_dir": None, # Lock this variable
                "output_kms_key": self._parse_ref(stage_config.get("kms_key", None)),
                "environments": self._parse_ref(stage_config.get("env", {})),
                "tags": self.exp_config["project_info"].get("tags", []),
                "hyperparameters": {
                    k: self._parse_ref(v) for k, v in 
                    stage_config.get("hyperparameters", {}).items()
                }, # hyperparam is a dict
                "entry_point": stage_config.get("entry_point"),
                "source_dir": stage_config.get("source_dir"),
                "sagemaker_session": "TBD"
            }
            stage_config["run_config"] = {
                "wait": True,
                "job_name": job_name
            }
            # parse input channel file paths
            for channel_name, input_item in stage_config["inputs"].items():
                stage_config["inputs"][channel_name]["source"] = self._parse_ref(
                    input_item["source"]
                )
            # FIXME: unsure about this in framework other than TensorFlow yet
            if self.local is False:
                stage_config["model_artifact_path"] = os.path.join(
                    stage_config["estimator_config"]["output_path"],
                    job_name,
                    "output/model.tar.gz"
                )
            else:
                stage_config["model_artifact_path"] = os.path.join(
                    stage_config["estimator_config"]["output_path"],
                    job_name,
                    "model.tar.gz"
                )
            # validation of config
            pass
        stage_config = {k: v for k, v in stage_config.items() if k in self._retain_stage_config_attrs}
        return stage_config

    def _parse_ref(self, ref_str):
        if ref_str is not None and type(ref_str) == str:
            if ref_str.startswith("$"):
                path_pieces = ref_str.split("/")
                anchor_var = self.exp_config
                rest_path = []
                for idx, piece in enumerate(path_pieces):
                    if piece.startswith("$"): 
                        anchor_var = anchor_var[piece.lstrip("$")]
                    else:
                        rest_path = path_pieces[idx:]
                        ## when find the part of path that not contains the reference sign
                        ## output the concatenated
                        # print(path_pieces, anchor_var, rest_path, os.path.join(anchor_var, *rest_path))
                        return os.path.join(anchor_var, *rest_path)
                return anchor_var
            else:
                return ref_str
        else:
            return ref_str
    
if __name__ == "__main__":
    print("Loading the pipeline_config.yaml to setup the experiment and export to artifacts/exp.json")
    os.makedirs("artifacts", exist_ok=True)

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--role", 
        help="role used to execute the pipeline",
        type=str
    )
    parser.add_argument(
        "--default_exp_bucket", 
        help="job type of this stage",
        type=str,
    )
    parser.add_argument(
        "--commit_sha",
        help="The commit revision the project is built for",
        type=str,
    )
    parser.add_argument(
        "--default_region",
        help="default region that this pipeline would use",
        type=str,
        default="us-east-1"
    )
    parser.add_argument(
        "--local",
        help="use local mode to initiate this experiment",
        type=bool,
        default=False
    )
    args = parser.parse_args()

    session = boto3.Session(region_name=args.default_region)
    sm_client = session.client("sagemaker")
    s3_client = session.client("s3")

    exp = AWSSMGitExperiment(
        role=args.role,
        default_exp_bucket=args.default_exp_bucket,
        commit_sha=args.commit_sha,
        region=args.default_region,
        local=args.local
    ).read_exp_config(
        config_path="pipeline_config.yaml"
    )
    exp.export("artifacts/exp.json")