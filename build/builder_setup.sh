#!/bin/bash
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip >/dev/null # silence the install of aws
./aws/install
echo $(aws --version)
echo $(python3 -m pip --version)
python3 -m pip install sagemaker boto3 botocore pyyaml psutil
alias python=python3
echo "Build for $CI_COMMIT_SHA"
echo "=========== Scan Files ==========="
ls -las ./*
echo "=========== Scan Files End ==========="