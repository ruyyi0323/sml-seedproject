#!/bin/bash

python build/exp_utils.py \
--role <role to execute the gitlab job> \
--default_exp_bucket <default project bucket> \
--default_region <default sagemaker region> \
--local true

python build/submit_job.py --stage prepare 

python build/submit_job.py --stage dataeval 

python build/submit_job.py --stage featurize

python build/submit_job.py --stage train

python build/submit_job.py --stage modeleval